Rails.application.routes.draw do
  get 'results_shops/index'

  get 'search_shops/index'

  get 'welcome/index'

  resources :shops

  root 'welcome#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
