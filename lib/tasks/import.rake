require 'csv'
namespace :import do
    desc "Import shops from csv"
    task :create_shops => :environment do
        filename = "db/shops_shopmium.csv"
        # filename = File.join Rails.root, "db\shops_shopmium_light.csv"
        CSV.foreach(filename, :headers => true) do |row|

            Shop.create(chain: row[0], name: row[1], latitude: row[2], longitude: row[3], address: row[4], city: row[5], zip: row[6], phone: row[8], country_code: row[18])

            # Fonctionne uniquement s'il n'y a pas d'espace entre les headers
            # Shop.create(chain: row['chain'], name: row['name'], latitude: row['latitude'], longitude: row['longitude'], address: row['address'], city: row['city'], zip: row['zip'], phone: row['phone'], country_code: row['country_code'])

        end
    end
end
