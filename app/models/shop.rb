class Shop < ApplicationRecord
    include Filterable

    scope :city, -> (city) { where city: city }

    geocoded_by :full_address # full_address is a method which take some model's attributes to get a formatted address for example

    # the callback to set longitude and latitude
    after_validation :geocode

    def full_address
        "#{address}, #{zip}, #{city}, #{country_code}"
    end

    def gmaps4rails_address
        "#{address}, #{zip}, #{city}, #{country_code}"
    end
end
