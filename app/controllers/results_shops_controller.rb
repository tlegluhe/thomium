class ResultsShopsController < ApplicationController
    def index
        if params[:location].present?
            @critere = "recherche par lieu"
            @location = params[:location]
            @nombre = (params[:nombre].present? ? params[:nombre] : 5)
            @distance = (params[:distance].present? ? params[:distance] : 10)

            # @shops = Shop.near(params[:location], params[:distance] || 10, order: :distance)
            @shops = Shop.near(@location, @distance, :units => :km).limit(@nombre)
            # @shops = Shop.near("rue Winston Churchill", 10, :units => :km).limit(5)

        elsif params[:latitude].present? && params[:longitude].present?
            @nombre = (params[:nombre].present? ? params[:nombre] : 5)
            @critere = "recherche par coordonnées"
            arr = [params[:latitude], params[:longitude]]
            @shops = Shop.near(arr, 1000).limit(@nombre)
        else
            @critere = "pas de critère"
            @shops = Shop.all
        end
        @trouve = @shops.length != 0
        @hash = Gmaps4rails.build_markers(@shops) do |shop, marker|
            marker.lat shop.latitude
            marker.lng shop.longitude
            marker.title shop.name
            marker.infowindow shop.name
        end
    end
end
