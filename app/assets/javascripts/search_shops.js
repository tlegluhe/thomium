function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        alert("Impossible de se géolocaliser");
    }
}

function showPosition(position) {
    document.getElementById("latitude").value = position.coords.latitude;
    document.getElementById("longitude").value = position.coords.longitude;
    console.log(position.coords.latitude+", "+ position.coords.longitude);
    // window.location = "/results_shops/index?utf8=✓&latitude="+position.coords.latitude+"&longitude="+position.coords.longitude+"&commit=Rechercher"
}
