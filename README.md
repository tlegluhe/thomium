# README

## Versions

* Ruby 2.3.3
* Rails 5.1
* SQLite 3.8.7.2

## Clone this repository

`git clone git@gitlab.utc.fr:tlegluhe/thomium.git`

cd to cloned repository
`cd thomium`

## Install dependencies

`bundle install`

## Create and populate database

`rails db:migrate`

`rake import:create_shops`

`rake geocode:all CLASS=Shop`

## Launch web server

`rails server`
